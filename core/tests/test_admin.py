from django.contrib.auth import get_user_model
from django.test import Client
from django.test import TestCase
from django.urls import reverse


class AdminSiteTests(TestCase):
    """Classe que permet gestionar usuaris/superusuaris des de l'entorn web"""

    def setUp(self):
        """
            Configuració del Test
            Crea un client (Client) que utilitzarem per a la realització dels Tests
            D'aquesta manera no manipularem la base de dates del projecte
        """
        self.client = Client()
        # Creem un super-usuari per no haver de crear-ne un a cada test
        self.admin_user = get_user_model().objects.create_superuser(
            email='admin@ies-eugeni.cat',
            password='password123'
        )
        # https://docs.djangoproject.com/en/4.0/topics/testing/tools/#django.test.Client.force_login
        # D'aquesta manera forcem l'autenticació (no cal autenticar-se)
        self.client.force_login(self.admin_user)
        # Creem un usuari per no haver de crear-ne un a cada test
        self.user = get_user_model().objects.create_user(
            email='test@ies-eugeni.cat',
            password='password123',
            name='Test Admin User Full Name',
        )

    def test_usuaris_llistats(self):
        """Test: comprova que els usuaris estan llistats a la pàgina
        d'usuaris"""
        url = reverse('admin:core_user_changelist')
        res = self.client.get(url)

        self.assertContains(res, self.user.name)
        self.assertContains(res, self.user.email)

    def test_usuari_canvia_pagina(self):
        """Test: comprova que la pàgina d'edició d'usuaris funciona"""
        url = reverse('admin:core_user_change', args=[self.user.id])
        # /admin/core/user/<id>
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)  # Codi 200 --> Tok Ok

    def test_crea_pagina_usuari(self):
        """Test: comprova que la creació d'una pàgina d'usuari funciona"""
        url = reverse('admin:core_user_add')
        res = self.client.get(url)

        self.assertEqual(res.status_code, 200)  # Codi 200 --> Tok Ok
