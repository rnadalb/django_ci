import unittest

from django.contrib.auth import get_user_model

from core import models


def usuari_exemple(email='test@ies-eugeni.cat', password='testpass'):
    """Crea un usuari d'exemple per facilitar els tests"""
    return get_user_model().objects.create_user(email, password)


class ModelTest(unittest.TestCase):
    def test_crear_usuari_amb_email(self):
        """Test: crea un nou usuari"""
        email = 'test@ies-eugeni.cat'
        password = 'Password123'
        user = get_user_model().objects.create_user(
            email=email,
            password=password
        )

        self.assertEqual(user.email, email)
        self.assertTrue(user.check_password(password))

    # def test_crear_usuari_amb_email_duplicat(self):
    #     """Test: crea un usuari amb el mateix email"""
    #     with self.assertRaises(IntegrityError):
    #         get_user_model().objects.create_user('test@ies-eugeni.cat', "test1234")

    def test_normalitza_email(self):
        """Test: normalitza un correu electrònic"""
        email = "test2@IES-EuGeNi.CaT"
        usuari = get_user_model().objects.create_user(email, 'test123')
        self.assertEqual(usuari.email, email.lower())

    def test_crear_usuari_amb_email_invalid(self):
        """Test: crea un usuari amb un correu electrònic erroni"""
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, "test1234")

    def test_crea_nou_superusuari(self):
        """Test: crea un nou usuari administrador"""
        user = get_user_model().objects.create_superuser(
            email='super@ies-eugeni.cat',
            password='Password123'
        )

        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

    def test_crea_nou_dispositiu(self):
        """Test: crea un dispositiu nou"""
        name = 'Dispositiu 1'
        dev = models.Device.objects.create(
            user=usuari_exemple(),
            name=name,
        )

        # self.assertEqual(dev.user, user)
        self.assertEqual(dev.name, name)
