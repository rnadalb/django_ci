from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Convenció per les traduccions de idiomes
from django.utils.translation import gettext as _

from core import models


class UserAdmin(BaseUserAdmin):
    ordering = ['id']  # Ordre a la visualització
    list_display = ['email', 'name', 'is_active']  # Quines dades es mostren a la llista
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        # Secció Informació Personal
        (_('Personal Info'), {'fields': ('name',)}),
        (
            # Secció Permisos
            _('Permissions'),
            {
                'fields': (
                    'is_active',
                    'is_staff',
                    'is_superuser',
                )
            }
        ),
        # Secció Important Dates
        (_('Important dates'), {'fields': ('last_login',)}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }),
    )


# Register your models here.
admin.site.register(models.User, UserAdmin)
admin.site.register(models.Device)
