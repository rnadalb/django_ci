from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin
)
from django.db import models, IntegrityError


# Create your models here.
class UserManager(BaseUserManager):  # Create your models here. # Create your models here.
    def create_user(self, email, password=None, **extra_fields):
        """
        Crea i emmagatzema un usuari
        :param email: Correu electrònic de l'usuari
        :param password: Contrasenya de l'usuari
        :param extra_fields: Camps extra, per si cal afegir-nes més al model
        :return: L'usuari creat
        """
        if not email:
            raise ValueError('Els usuaris han de tenir un correu electrònic')
        usuari = self.model(email=self.normalize_email(email), **extra_fields)
        usuari.set_password(password)
        try:
            usuari.save(using=self._db)
        except IntegrityError:
            pass
        return usuari

    def create_superuser(self, email, password):
        """
        Crea un superusuari
        :param email: Correu electrònic del super usuari
        :param password: Contrasenya del super usuari
        :return: el super usuari creat
        """
        usuari = self.create_user(email=email, password=password)
        usuari.is_superuser = True
        usuari.is_staff = True
        usuari.save(using=self._db)
        return usuari


class User(AbstractBaseUser, PermissionsMixin):
    """ Model d'usuari personalitzat que suporta l'autenticació per
        email en lloc de per username"""
    email = models.EmailField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'


class Device(models.Model):
    """Model que representa un Dispositiu"""
    name = models.CharField(max_length=255, null=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,  # D'aquesta manera ens assegurem els possibles canvis a nivell global
        on_delete=models.CASCADE,  # Si eliminem un usuari, eliminem tots els seus dispositius
    )

    # https://stackoverflow.com/questions/1196415/what-datatype-to-use-when-storing-latitude-and-longitude-data-in-sql-databases
    latt = models.DecimalField(max_digits=8, decimal_places=6, null=True)  # --> ##.######
    long = models.DecimalField(max_digits=9, decimal_places=6, null=True)  # --> ###.######

    def __str__(self):
        return f'{self.name}: [{self.latt}, {self.long}]'
